class  Profesora 
	attr_reader :codigo,:dni,:nombre,:fechaNacimiento,:especialidad

	def initialize(codigo,dni,nombre,fechaNacimiento,especialidad)
			@codigo,@dni,@nombre,@fechaNacimiento,@especialidad = codigo,dni,nombre,fechaNacimiento,especialidad
	end

	def  calcularSueldoNeto()
		return  0
	end

	def  mostrarDatos() 
		puts""  
		puts "Codigo :#{@codigo} " 
		puts "DNI :#{@dni} "
		puts "Profesora :#{@nombre}" 
		puts "Fecha Nacimiento :#{@fechaNacimiento}" 
		puts "Especialidad :#{@especialidad}"
		puts "Calcular Sueldo Neto :#{calcularSueldoNeto()}" 

	end

  	def obtenerModalidadTrabajo()
    #  return "Profesora"
  	end

end

class TiempoCompleto <Profesora
	 attr_reader :sueldoFijo, :fondoPension
	def  initialize(codigo,dni,nombre,fechaNacimiento,especialidad,sueldoFijo,fondoPension)
		super(codigo,dni,nombre,fechaNacimiento,especialidad)
		@sueldoFijo =sueldoFijo
		@fondoPension =fondoPension

	end

	def  calcularSueldoNeto()
		return  sueldoFijo
	end

	def  mostrarDatos()
		super()
		puts "Sueldo Fijo :#{sueldoFijo}"
		puts "Fondo Pensión :#{fondoPension}"
	end

	def obtenerModalidadTrabajo()
      return "TiempoCompleto"
  	end
end


class TiempoParcial < Profesora
	attr_reader :horas ,:tarifaHora
	def  initialize(codigo,dni,nombre,fechaNacimiento,especialidad,horas,tarifaHora)
		super(codigo,dni,nombre,fechaNacimiento,especialidad)
		@horas	= horas 
		@tarifaHora =  tarifaHora
	end 

	def  calcularSueldoNeto()
		return  horas*tarifaHora
	end

	def  mostrarDatos()

		super()
		puts "Horas :#{horas}" 
		puts "TarifaxHora :#{tarifaHora}"
	end

	def obtenerModalidadTrabajo()
      return "TiempoParcial"
  	end

end 


