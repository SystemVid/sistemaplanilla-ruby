require_relative "clsProfesora.rb"

class Administradora
	#attr_reader :profesora
	def initialize()
		@profesora = []
	end 

	def registrarProfesora(profesoraDatos)
	   if buscarProfesora(profesoraDatos.dni) !=nil
	     return  raise "Error ya existe la profesora con DNI: "+ profesoraDatos.dni
	    end 
	     return   @profesora.push(profesoraDatos)
  	end 

  	def buscarProfesora(dni)
     for profesora  in @profesora
        if profesora.dni == dni
          return profesora
        end
     end
      return nil 

    end

	def  obtenerProfesoras()
		return @profesora
	end 

	def obtenerProfesoraServicio(tipoServicio)
	    resultado = []    
	    for servicio in @profesora
	      if servicio.obtenerModalidadTrabajo() == tipoServicio
	        resultado.push(servicio)
	      end
	    end    
	    return resultado
    end 

    def obtenerTotalPagar
    	 monto = 0    
	    for total in @profesora
	    	monto +=total.calcularSueldoNeto()
	    end    
	    return monto
    end

    def obtenerProfesoraMayorSueldo()
	    mayorSueldo = 0
	    profesoraMayorSueldo = nil    
	    for sueldo in @profesora
	      if sueldo.calcularSueldoNeto() > mayorSueldo
	        mayorSueldo = sueldo.calcularSueldoNeto()
	        profesoraMayorSueldo = sueldo
	      end 
	    end    
	    return profesoraMayorSueldo
  	end
	

	def obtenerProfesoraPromedioSueldo()
	    mayorSueldo = 0
	    cont = 0
	    for sueldo in @profesora 
	        mayorSueldo += sueldo.calcularSueldoNeto()
	    	cont +=1
	    end    
	    return (mayorSueldo/cont)
  	end

end 

objAdmin = Administradora.new(); 

profCompleto1 = TiempoCompleto.new("COD-5444","78965485","ROSA FLORES SILVERA","12/05/1998","systema",2500,250)
profCompleto2 = TiempoCompleto.new("COD-5448","78445454","JUANA FLORES SILVERA","12/05/1997","INFORMATICA",3000,350)

profParcial1 = TiempoParcial.new("TPAR-5478","7458948","DANA FLORES SILVERA","12/05/1992","LENGUAJE",95,35.8)
profParcial2 = TiempoParcial.new("TPAR-5448","7458948","PAOLA FLORES SILVERA","12/05/1992","MATEMÁTICA",100,35)



begin  objAdmin.registrarProfesora(profCompleto1)
rescue Exception =>e  
	puts e.message
end

begin  objAdmin.registrarProfesora(profCompleto2)
rescue Exception =>e  
	puts e.message
end
begin  objAdmin.registrarProfesora(profParcial1)
rescue Exception =>e 
 puts e.message
end
begin  objAdmin.registrarProfesora(profParcial2)
rescue Exception =>e 
 puts e.message
end


puts "**********A *******Listando profesoras ************************"
dataProf = objAdmin.obtenerProfesoras() 

for lista in dataProf
   lista.mostrarDatos()
   puts "---------------------- ------"
end


puts "*********B ******Listando Tipo de Servicio Profesoras *********"
dataServicio = objAdmin.obtenerProfesoraServicio("TiempoParcial") 

for lista in dataServicio
   lista.mostrarDatos()
   puts "---------------------- ------"
end




puts "*********C ******Monto Total Salario Mayor de Profesoras  *********"
dataSueldo = objAdmin.obtenerProfesoraMayorSueldo()
dataSueldo.mostrarDatos()

puts "*********E ******Monto Total Salario del Mes Profesoras  *********"
puts "Monto Total Pagar :#{objAdmin.obtenerTotalPagar()}" 

puts "*********D ****** Promedio de Sueldo Profesoras  *********"
puts "Sueldo Promedio :#{objAdmin.obtenerProfesoraPromedioSueldo()}" 
 
